//
//  getString.c
//
//
//  Created by Mikhail Yankelevich on 23/04/2018.
//


#include   "main.h"
/* this function is an analg of the GETLINE of c++ with changing to 0 if nothing is entered + all spases changed to _( used it as the rogram used to be in c++*/
void getString (char *c)
{
    
    fgets(c, 60, stdin);// entering an artist name
    if (c[0]!='\n'){
    long int i=strlen(c)-1;// changing the last symbot of the string to /0 by finding the last sybol in the string
    if(c[i] == '\n')
        c[i] = '\0';
    
    for (int j =0 ; j<strlen(c); j++)//changing all spaces to _ so the file would be rearable
        if (c[j]==' ')
            c[j]='_';
    }
    else//if nothing entered writing 0 to the file
    {c[0]='0';
    c[1]='\0';
    }
}



/*this function recognises the last id in the list (which is the total number of the objects, if none is deleted, which is not possible to do by user)*/
void finallIdRecognize(int *IdMax)
{
    struct Odezda o;//initialising struct variubles
    FILE *Dat;//openning the file
    Dat=fopen(datFile,  "r");//openning for reading only, as there is no need for anything else, just scanning
    while(!feof(Dat))
    {//scanning info till the end of hte file
        fscanf(Dat, "%d%s%s%s%s%s%s%s%s%s%s%s%d%d\n",&o.Id,o.trusiki,o.lifchik,o.kolgotki,o.top,o.pants,o.jacket,o.scarf,o.hat,o.shose,o.bag,o.mesura,&o.weather,&o.hide);//scanning the whole line
        
       if (developer==1)//in case of the developer mode turned on
           printf("%d: \nТрусики : %s\nЛифчик : %s\nКолготки/чулки : %s\nТоп : %s\nШтаны/юбка : %s\nКуртка : %s\nШарф : %s\nШапка : %s\nБотинки : %s\nСумка : %s\nАксессуары : %s\n\n\n",o.Id,o.trusiki,o.lifchik,o.kolgotki,o.top,o.pants,o.jacket,o.scarf,o.hat,o.shose,o.bag,o.mesura);//debug line
        
    }
    
    fclose(Dat);//closing the file
    *IdMax=o.Id;//passing a max id
}
