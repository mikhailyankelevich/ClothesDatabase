////
////  main.h
////
////
////  Created by Mikhail Yankelevich on 23/04/2018.
////
//
//#ifndef main_h
//#define main_h
//
//
//#endif /* main_h */
#include <stdio.h>
#include  <time.h>
#include  <stdlib.h>
#include  <string.h>
#import <Cocoa/Cocoa.h>

struct Odezda{
    char trusiki[60];
    char lifchik[60];
    char kolgotki[60];
    char top[60];
    char pants[60];
    char jacket[60];
    char scarf[60];
    char hat[60];
    char shose[60];
    char bag[60];
    char mesura[60];
};

void menu(void);
void fileRecord(void);
void printOutEverything(void);
void printOutRandom(void);
//void choiceProcedure(FILE *Database, int Id);
void getString(char *c);
void finallIdRecognize(void);
void rmElement(void);
int idNumber;
@interface ViewController : NSViewController


@end
